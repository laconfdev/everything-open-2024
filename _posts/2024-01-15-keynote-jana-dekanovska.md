---
layout: post
title: "Keynote announcement - Jana Dekanovska"
card: card-keynote-dekanovska.png
---

We are delighted to announce our second Keynote speaker at Everything Open 2024, Jana Dekanovska.
{:.lead}

Jana Dekanovska is a well-known threat intelligence analyst, with a particular interest in the intersection of artificial intelligence (AI) and cyber security. She serves as a Strategic Threat Advisor at [CrowdStrike](https://www.crowdstrike.com/en-au/), where she holds responsibility for their cyber threat intelligence business across Australia and Aotearoa/New Zealand. Jana’s keynote will examine the rise of artificial intelligence within cyber security, and in particular how Adversarial AI is altering the cyber security threat landscape - and what you can do about it. 

Registrations are now open for Everything Open 2024, and we have curated a list of travel and accommodation options for you. You may be surprised at how affordable accommodation in Gladstone is, particularly compared to regional cities, with many options under $AUD 150 a night.

<a href="../../dashboard/" class="btn btn-primary" role="button">Register now!</a>

<a href="../../attend/tickets/" class="btn btn-primary" role="button">View ticket prices</a>

<a href="../../attend/travel-accommodation/" class="btn btn-primary" role="button">View travel and accommodation options</a>

## About Jana

{% include keynote_image.html url="../../media/img/keynotes/jana-dekanovska.jpg" description="Image: Jana Dekanovska (supplied)" %}

Jana focuses on helping customers operationalize and integrate threat intelligence within their organization’s security strategy, demonstrating the value of CrowdStrike intelligence capabilities.

Prior to her current role, Jana was a Senior Cyber Threat Intelligence Analyst at CyberCX, developing a wide variety of analytical products across strategic, operational and tactical levels of intelligence for a diverse customer base. Before moving to Australia, Jana held multiple intelligence-based roles across Merck and Amazon in the Czech republic, and served in NATO commands across Italy, Belgium and the Netherlands.

Jana regularly presents at events in Australia and internationally and is frequently interviewed by Australian media on cyber security news topics.

## About the keynote 

AI is a hot topic in cyber security. Everyone is curious about it. Excited about its use cases and nervous about the problems it may cause in the wrong hands. AI as a tool can be used by both defenders and adversaries. This talk will focus mainly on what the adversaries are doing from the offensive perspective and unpack the concept of Adversarial AI that will fundamentally change the threat landscape and lower the barrier of entry for adversaries to enter the cyber security game.

The underlying openness of AI means that we can now take the collective knowledge of many smart people and bend the time and knowledge gap, giving this advantage to the adversary, creating threat actors that are smarter, faster and have the ability to do more damage. AI is going to change the threats as we know them today and is the next big problem we are facing in security. 

Jana will discuss how adversaries are using AI today, how it could be used in the future, and how this will affect your business.

You can keep up to date with all the Everything Open happenings in the following ways:

{% include socials.html %}