---
layout: post
title: "Brisbane to Gladstone bus to help you get to Everything Open 2024"
card: card-brisbane-gladstone-bus.png
---

<p class="lead">Bus seats now available to order from the Tickets page.</p>

Although there are [many ways to get to Gladstone for Everything Open 2024](/attend/travel-accommodation), we heard from many potential Delegates that airfares between Brisbane (BNE) and Gladstone (GLT) were cost prohibitive, dampening enthusiasm to attend `#EverythingOpen` 2024. 

While we can't spin up a competing airline (Penguin Air, anyone?), we _can_ put on a bus from Brisbane to Gladstone, and return from Gladstone to Brisbane, to get you to and from the conference. 

## Schedule and pick up / departure points 

* The bus will leave from Brisbane, close to [Brisbane airport (BNE)](https://www.bne.com.au/), on Monday 15th April at 1030hrs AEST. 
* The bus will travel to Gladstone, alighting close to the conference venue approximately 8 hours later at 1830hrs, traffic dependent. There will be a brief meal stop during the journey. This timing allows you to check into your [accommodation](/attend/travel-accommodation/) and get a good night's sleep before the conference starts the following day. 
* On the return journey, the bus will leave from Gladstone on Friday 19th April at 0700hrs AEST sharp, from near the conference venue and will arrive in Brisbane, close to [Brisbane airport (BNE)](https://www.bne.com.au/) approximately 8 hours later. There will be a brief meal stop during the journey. 
* The bus has an onboard toilet and has onboard Wi-Fi, although we cannot guarantee Wi-Fi for the whole journey. 

## Cost 

Bus tickets will cost $AUD 40 each way, which is a significant saving compared to flights between Brisbane and Gladstone. You can purchase bus tickets via the Dashboard. 

<a href="/dashboard/" class="btn btn-primary" role="button">Buy bus tickets now</a>

## Keep in touch 

You can keep up to date with all the Everything Open happenings in the following ways, or email us at [contact@everythingopen.au](mailto:contact@everythingopen.au)

{% include socials.html %}

