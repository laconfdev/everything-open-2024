---
layout: post
title: "Registration is now available for Everything Open 2024 - powering the future"
card: card-announce-registrations-open.png
---

<p class="lead">We’re delighted to announce that registration for Everything Open 2024 is now available.</p>

<a href="../../dashboard/" class="btn btn-primary" role="button">Register now!</a>

[Register now](../../dashboard/) to attend Everything Open 2024, in beautiful Gladstone, tropical north Queensland. We have a range of [ticket prices](../../attend/tickets/) suitable for most budgets, and have compiled [information on travel to, and accommodation in Gladstone](../../attend/travel-accommodation/), as well as [information on what you can do while you're here](../../attend/gladstone/). We're looking forward to seeing you in April 2024!

In recognition of both the current cost of living situation, and that Gladstone requires additional travel for most delegates, we have decided to keep ticket prices the same as Everything Open 2023. This would not be possible without our generous [sponsors](../../sponsors).

<a href="../../attend/tickets/" class="btn btn-primary" role="button">View ticket prices</a>

If you have a requirement to reconcile ticket purchases before the end of the year, we’re very happy to assist you in doing so, and recommend you reach out to us at [contact@everythingopen.au](mailto:contact@everythingopen.au?Concierge service) so we can provide a concierge service.

Everything Open still represents one of the best value for money, technically transdisciplinary conferences in Australia and Aotearoa New Zealand, bringing you industry luminaries, dozens of talks from experts, opportunities to make and renew connections with fellow practitioners, and invaluable insights into how everything open is powering the future.

While Everything Open is reason enough to get to Gladstone in April, we’ve also [curated information on other activities you might like to partake in as part of your trip](../../attend/gladstone/) - including the stunning [Heron Island](https://www.heronisland.com/). We also have all of your [travel and accommodation information covered here](../../attend/travel-accommodation/).

So, join the region’s open source, open data, open GLAM, open gov, open science, open hardware and other open practitioners, and power up today!

You can keep up to date with all the Everything Open happenings in the following ways:

{% include socials.html %}

## Sponsor Early

As usual, we have a range of sponsorship opportunities available, for the conference overall as well as the ability to contribute towards specific parts of the event.
We encourage you to sponsor the conference early, to get the maximum promotion during the lead up to the event.
If you or your organisation is interested in sponsoring Everything Open, please [get in touch](../../sponsors/prospectus/).











