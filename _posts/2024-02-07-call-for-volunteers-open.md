---
layout: post
title: "Call for Volunteers Now Open"
card: card-call-for-volunteers.png
---

<p class="lead">Lend your energy, expertise and excellence to help make Everything Open happen on the ground!</p>

## Call for Volunteers

Are you great with people? Have some familiarity with audio visual technologies? Confident addressing an audience? Able to use chat systems, online document sharing and social media? 

Then we'd love to have you on board as a Volunteer on the ground in Gladstone in mid-April to make the magic happen. 

If you're a regular Volunteer at Everything Open and just want to skip to the sign up - firstly, thank you! 

<a href="https://forms.gle/Vfnu7nSfuNHkMQxn6" class="btn btn-primary" role="button">Regular? Apply here to be a Volunteer at Everything Open 2024</a>

If you’re new to it all and want to help out, keep reading!

We’re all volunteers ourselves and we need more – we can’t run the conference by ourselves, particularly during the week of the conference itself. 

We need help with:

* Checking attendees in when they arrive
* Operating AV equipment such as audio gear and cameras
* Directing people around our venue
* Ensuring talks and tutorials run to schedule
* Setting up and packing up the conference

Anyone who has volunteered for a [Linux Australia event](https://linux.org.au/events) before will tell you it’s a very busy time, but also very worthwhile. It’s satisfying to know that you’ve helped everyone at the conference to get the most out of it. It’s very rewarding knowing that you’ve made a positive difference to someone’s day.

You don’t just get to meet the Delegates and Speakers, you also get to know many of them while helping them out. You are presented with a unique opportunity to get the behind the scenes and close to the action. You’ll get to forge new relationships with amazing, interesting and wonderful people (just like you), whom you might not have otherwise had the good fortune to meet in any other way.

In return for your help we’ll provide you with:

* Food - morning tea, lunch and afternoon tea
* A clean T shirt everyday
* If you want one, a letter of reference at the end of the conference

Depending on the number of Volunteers we get and workload (many hands make light work), we’ll do our best to allocate you to roles so that you can attend talks that interest you.

For more information, please check out our [Volunteers](/attend/volunteer/) page, where we have full details of what we need assistance with. We review and approve applications regularly.

You can keep up to date with all the Everything Open happenings in the following ways:

{% include socials.html %}

## Sponsor Early

As usual, we have a range of sponsorship opportunities available, for the conference overall as well as the ability to contribute towards specific parts of the event. We encourage you to sponsor the conference early, to get the maximum promotion during the lead up to the event. If you or your organisation is interested in sponsoring Everything Open, please [get in touch](../../sponsors/prospectus/).

