---
layout: post
title: "Locknote announcement - Rae Johnston"
card: card-keynote-johnston.png
---

<p class="lead">We are delighted to announce our award-winning Locknote speaker - Rae Johnston!</p>

## About Rae Johnston

Rae Johnston was born and raised on Darug & Gundungurra Country in the Blue Mountains of NSW with her Wiradjuri mother and Greek father. A multi-award-winning journalist and broadcaster, she has a passion for the geekier side of life.

{% include keynote_image.html url="/media/img/keynotes/rae-johnston.jpg" description="Image: Rae Johnston (supplied)" %}

Rae was the first Science & Technology Editor for NITV at SBS, and currently travels the country as a host on NITV's Going Places with Ernie Dingo, and ABC's Back Roads.

An accomplished podcast producer and host, Rae has worked on a slew of programs such as Harmful (SBS), 100 Climate Conversations (Powerhouse Museum) and Download This Show (ABC). She also hosts the Next Gen podcast with Unicef Australia, for whom she is an Ambassador.
Rae currently serves on the boards of the Telstra Foundation and Swinburne University of Technology, where she also sits on the Technology, Innovation, and Value Creation Committee.

## About Rae's Locknote - Who gets to work in STEM? And who is being left out?

As a science and technology journalist, Rae travels to every corner of the country, finding out what barriers exist to meaningfully including people from all walks of life in the worlds of science and tech. From societal bias and unsafe workplaces to burnout and the digital divide, this talk gets to the heart of what needs to be done so that everyone can play a role in shaping the future.

## Register now for Everything Open 2024!

[Register now](../../dashboard/) to attend Everything Open 2024, in beautiful Gladstone, tropical north Queensland. We have a range of [ticket prices](../../attend/tickets/) suitable for most budgets, and have compiled [information on travel to, and accommodation in Gladstone](../../attend/travel-accommodation/), as well as [information on what you can do while you're here](../../attend/gladstone/). We're looking forward to seeing you in April 2024!

<a href="../../dashboard/" class="btn btn-primary" role="button">Register now!</a>
<a href="../../attend/tickets/" class="btn btn-secondary" role="button">View ticket prices</a>

You can keep up to date with all the Everything Open happenings in the following ways:

{% include socials.html %}

