---
layout: post
title: "That's a wrap for Everything Open 2024!"
card: card-wrap.png
---

<p class="lead">That's a wrap for Everything Open 2024 - thank you for joining us!</p>

That's a wrap for Everything Open 2024! 

Thank you all so much for joining us in Gladstone - the first time that a [Linux Australia](https://linux.org.au) event has been run in Central Queensland - but hopefully, not the last! We hope you enjoyed `#EverythingOpen` as much as we did, and are keen to continue the momentum in [Adelaide in January](#everything-open-adelaide-2025). 

In this post, we're looking back on the [conference highlights](#conference-highlights), covering the [findings from our post-conference survey](#post-conference-survey-findings) and looking forward to [what's next](#thank-yous-and-next-steps). 

## Conference highlights 

Before the conference proper, the good folks at [APNIC](https://www.apnic.net) delivered routing security training. Samantha Douglas at APNIC [wrote up the team's experience at Everything Open on the APNIC blog](https://blog.apnic.net/2024/05/01/event-wrap-everything-open-2024/) - featuring an excellent photo of Community Trainer, Terry Sweetser. 

On Tuesday, we started proceedings with a Welcome to Country ceremony, acknowledging the traditional custodians of the lands upon which Everything Open 2024 was held - the Bailai, Gurang, Gooreng Gooreng and Taribelang Bunda Peoples. We were wished a warm "balka wittaberri" - welcome in Gooreng Gooreng language. We extend our thanks to the [Port Curtis Coral Coast Trust](https://www.pccctrust.com.au/) for sharing their language and for welcoming us to their lands.

{% include feature_image.html url="/media/img/geoff-huston-keynote.jpg" description="Geoff Huston keynoting Everything Open 2024 Gladstone, Christopher Biggs CC-BY, used with permission." floatdir="right" %}

We were then delighted to welcome [Geoff Huston AM](https://en.wikipedia.org/wiki/Geoff_Huston_(internet)), [Chief Scientist at the Asia Pacific Network Information Centre (APNIC)](https://www.apnic.net/), as our first keynote, where he spoke to the evolution of the internet from telegraphy to telephony, to the current state of affairs - and the challenges it presents for functions such as content distribution. Using the concept of "packet miles", Geoff explained the role of CDNs and the divergence between content networks and carriage networks, and questioned whether this structure is viable for the future.

Following Geoff's presentation, talks held on Tuesday included Clinton Roy's very popular talk on [TLA+](https://lamport.azurewebsites.net/tla/tla.html), a high-level language for modelling distributed and concurrent systems, PJ Waskiewicz's excellent overview of the compute express link (CXL) bus, Professor Gernot Heiser on Lions OS - a new operating system from the [Trustworthy Systems Group at UNSW](https://trustworthy.systems/), and Christopher Biggs on building an open source scooter to aid micro mobility. To round out the day, Daniel Black delivered an informative tutorial on [`rr`](https://rr-project.org/), a debugger for C/C++ multi-threaded applications.

On Wednesday, well-known threat intelligence analyst and Strategic Threat Advisor at [CrowdStrike](https://www.crowdstrike.com/en-au/), Jana Dekanovska, was our wonderful second keynote, where she examined the rise of artificial intelligence within cyber security, and showed how adversarial AI is altering the cyber security threat landscape. 

We were entertained a-plenty by the budding musicians of [Gladstone State High School](https://gladstonshs.eq.edu.au/)'s band, and you can [catch their performance here on YouTube](https://www.youtube.com/watch?v=voI8Ees8w08&feature=youtu.be).

Talks on Wednesday included Trent Lloyd on how to make benchmarking for storage repeatable, using `Ceph` as an example, Gustavo AR Silva on bounds-checking trailing arrays in the kernel to reduce vulnerabilities, Vik Olliver demonstrating his "Quirkey" accessible one-handed keyboard (which has already spawned some [replication projects](http://www.mawsonlakes.org/building-a-cheap-n-cheerful-quirkey-one-handed-keyboard/)), and William Brown covering replication strategies in `no-sql` databases. The Wednesday tutorial was given by Arjen Lentz, who provided an introduction to programming in the Go language.

{% include feature_image.html url="/media/img/wards-penguin-dinner.jpg" description="Image: Ward's Brewery, Gladstone, CC-BY, used with permission." floatdir="right" %}

Our Penguin Dinner was held at [Ward's Brewery](https://www.wardsbrewery.com/), where much merriment was had, and memories made.

{% include feature_image.html url="/media/img/aaron-quigley-keynote.jpg" description="Image: Professor Aaron Quigley keynoting Everything Open Gladstone, Christopher Biggs, CC-BY, used with permission." floatdir="left" %}

Professor Aaron Quigley, [Science Director and Deputy Director of CSIRO’s Data61](https://www.csiro.au/en/news/all/articles/2023/january/welcome-professor-aaron-quigley/), gave our amazing third keynote, where he covered advances in multi-modal sensor technology. Drawing from his teams' research in the field of human-communication interaction, he showed incredible advances in sensor-based technology - and, importantly - the impact these advances have for improving our lives through better care, health and ability supports.

Talks on Thursday included Felicity Brand on how FOSS has changed from tools to a social movement, Liz Quilty on the Rizzmaster 3000 - connecting girls with STEM careers, and Elton Shih on transitioning from FreeRTOS to ZephyrRTOS on ARM. Thursday's tutorial was provided by Maksim Lin, who showed us how to build the Picotracker - a fully open source groovebox. 

In the great Spaces vs Tabs debate, the real winner was the greyhounds at [Love a Greyhound](https://www.loveagreyhound.org.au) who will benefit from your generous donations - you raised over $1200!

{% include feature_image.html url="/media/img/spacesvstabs-greyhounds.jpg" description="Image: Spaces vs Tabs by Rob Thomas, CC-BY, used with permission." floatdir="right" %}

Rounding out the conference, we welcomed [Rae Johnston](https://raejohnston.com/), award winning journalist with NITV's Going Places with Ernie Dingo, and ABC's Back Road, who provided a resonant, heartfelt and moving story of her personal journey to a STEM career. She challenged us to think carefully about who gets the opportunity to participate in STEM, and how we can better support broadening our community. 

_Please note that we will publish conference videos, where we have permission from Speakers to do so, over the next few weeks. Thank you for your patience._

## Post-conference survey findings 

In the interests of continual improvement, we asked all Delegates, Volunteers and Organisers to fill out a short post-conference survey. 

{% include survey_image.html url="/media/img/survey/ticketsales-by-type.png" description="Figure 1: Ticket sales by type" floatdir="left" %}

In all, we had around 140 people physically at Everything Open 2024 - an outstanding number for a conference in a regional centre of just 60,000 people, a 7 hour drive from a capital city (for scale, Melbourne has a population just shy of 5 million, and Adelaide has around 1.5 million - 25 times more than Gladstone). Alternating a regional with a capital city location appears to strike the balance of remaining financially sustainable while ensuring Everything Open gets to as many parts of Oceania as possible. 

As Figure 1 shows, around a fifth of our Delegates joined online, confirming the demand for an online option at future conferences. Online streaming places additional audio visual demands on the volunteer organising team, and streaming will remain best effort going forward. Professional audio visual services are a significant cost, and engaging them in the future for streaming will remain dependent on attendance and sponsorship.

{% include survey_image.html url="/media/img/survey/surveyresponses-by-tickettype.png" description="Figure 2: Survey responses by ticket type" floatdir="left" %}

Overall, we had a response rate of 53%, which we're very grateful for. This response rate means we have a representative sample from all Delegate types. 

_Please note that we missed the "Speaker" ticket type off the Survey, so it is not represented in Figure 2. We assume most Speakers chose the "Professional" or "Contributor" ticket type._

{% include survey_image.html url="/media/img/survey/how-found-out-about-EO2024.png" description="Figure 3: How people found out about Everything Open 2024" floatdir="left" %}

Turning to how people found out about Everything Open 2024 in Figure 3, it is clear that the main channel for brand awareness is previous attendance. Two thirds of attendees had previously attended an Everything Open or linux.conf.au event, and that's how they found out about Everything Open 2024. Importantly, word of mouth referral was a key source of awareness, with nearly 1 in 5 Delegates finding out about the conference from a colleague or friend. Looking at the comments that were provided, the social media channels that are of most important here are Mastodon and LinkedIn, which tracks with the decline of X/Twitter. 

{% include survey_image.html url="/media/img/survey/confexperience-agreement-w-statements.png" description="Figure 4: Quality of the conference experience" floatdir="left" %}

Shown in Figure 4, we asked people to state their agreement with aspects of the conference, so we could gauge the quality of the offering. The Keynotes were very highly rated, as was the desire for the community to come together (this rating has no box because the answers were so tightly grouped, it's not an anomaly). The relevance of the program to Delegates ranked only marginally lower, as did the quality of the Speakers. Overall, we can state that the quality of the program was very high. 

{% include survey_image.html url="/media/img/survey/confrating.png" description="Figure 5: Meeting expectations" floatdir="left" %}

Next, as shown in Figure 5, we asked people to indicate whether some aspects of the conference met their needs, so we can adjust in the future. Here, it was very clear that the amount of catering supplied did not meet expectations, and there was dissatisfaction with how well dietary requirements were handled. Unfortunately, like most things, inflation means that catering costs have increased significantly, and keeping the Penguin Dinner cost to $100 per head for a sit down meal would be very challenging.

Additionally, Delegates indicated they would like more Tutorials in the program, and there was also a need identified for more social events in the program. The number of keynotes and number of talks in the program was about right. 

Looking at the written feedback that accompanied Figures 4 and 5, much of this corroborated the information in the graphs - a desire for more catering and more tutorials. We did not ask specifically about the length of the conference (Everything Open is three days, compared to linux.conf.au at five days), however written responses here were split - some people preferred the shorter format as it better allowed for work-life balance and was less onerous financially due to less nights of accommodation being required, while others indicated a desire to return to the five day format. The size of a conference is a function of volunteer capacity and ticket affordability: should a large conference team present a viable bid to run a five-day conference at a price point our community finds attractive, Linux Australia would likely endorse the bid. A three-day conference requires less volunteer effort.

There was a clear dislike of remotely-presented talks / pre-recorded talks. Unfortunately this situation was a combination of having a reduced budget to disburse for travel grants, and many employers choosing not to fund employee travel. This meant that once talks had been accepted into the program, the Speaker was unfunded to travel, and remote attendance provided a fallback. We hope to see this situation change as economic conditions improve. 

Several Delegates expressed a desire for talks to go into more depth, and we will provide this feedback to the Session Selection Committee to inform the Call for Sessions process. 

Next, we asked for written feedback about the conference Schwag [available on Red Bubble](https://www.redbubble.com/shop/ap/158192555). In general, most Delegates did not purchase anything, citing postage costs as the key barrier, particularly for smaller purchases like stickers. There was a general desire for t-shirts to be included in the ticket price, and we will consider whether this is viable given volunteer capacity going forward.

{% include survey_image.html url="/media/img/survey/attendrecommend.png" description="Figure 6: Attending or recommending a future Everything Open event" floatdir="left" %}

Turning to whether people would attend an Everything Open conference or recommend one to a colleague in the future (see Figure 6), we found a very high likelihood of both - which bodes well for the future! 

Looking at the written feedback on what worked well, and what didn't, these in general corroborated the above findings. 

The difficulties we experienced in providing online streaming were noted, and we have taken this on board as an area in which we need a more robust and repeatable solution, in line with our budget. Additionally, some Delegates commented on language used at the conference they weren't familiar with - such as "Birds of a Feather" and "Penguin Dinner". In the past we have run a Newcomers' Session where these terms are explained, however didn't have anyone nominate to run one this year. This data point indicates the Newcomers' Session is valuable. Several Delegates noted that getting to Gladstone was difficult and expensive to get to. We will take this on board when assessing future conference bids. We note that Gladstone was the only bid received for Everything Open 2024 - having a conference is dependent upon bids being submitted by a potential conference team, and we warmly encourage more bids to host Everything Open.

There were several positive comments about the conference overall, with Delegates very appreciative of volunteer efforts.

* "Just have to say that all the Keynotes were exceptional."
* "It was an awesome conference."
* "Definitely connected with a lot more people this year. It seems because we weren't in a city like Melbourne with so many places to go, a lot of people stuck around rather than splitting and disappearing into the city. Hopefully there is a way to encourage this next year in Adelaide!"
* "Very good variety of topics."
* "Thank you so much for putting this event together for the open source community. You did an incredible job, so hats off to you all."
* "Huge thanks to all organisers and volunteers, we love your work! 💕" 
* "I really appreciate the care you took of the participants. Travelling alone, I was thinking of my personal safety a few times and really glad to  be chauffeur driven around. It must have been a big effort for you all but it was very much warmly received and so big, big thank you for that!  The venue was awesome- really professional, love it. The keynotes were great gets! Absolutely loved the wifi - thank done and I’ll be back next year." 
* "I know this conference (and other under the auspice of Linux Australia) are run by volunteers. You all do an amazing job, and I'm sure that I speak for all delegates that we really appreciate the excellent work you do."
* "Pulling off a conference this complex in Gladstone is impressive and the team has done well. Even the additional help around getting people to/from Brisbane and airport transfers shows the passion of the community. " 


## Thank yous and next steps 

{% include feature_image.html url="/media/img/closing-organiser.jpg" description="Image: Everything Open 2024 Organising Team, Gustavo AR Silva, CC-BY, used with permission." floatdir="left" %}

Everything Open would not happen without a huge amount of voluntary effort from many people. Our Organising Team worked diligently 6 months to a year out from the conference, co-ordinating venue, sponsors, speakers, keynotes, ticket sales, communications and media, audio visual and much, much more. During the conference itself, our Volunteers provided Registration Desk, Room Monitoring, Audio Visual and Streaming, ushering and many more tasks too numerous to mention. Thank you. 

We received lots of local support from Gladstone, and in particular we'd like to thank Natalia Muszkat, Deputy Mayor of Gladstone Regional Council, for all her assistance. We'd like to thank Gladstone State High School for providing the lovely musical interlude. Gladstone Entertainment and Conference centre was a wonderful venue for Everything Open, and we'd like to thank the team for all their efforts.

A huge thank you to our [Sponsors](/sponsors) - Google, ARM, Open Source Institute, RedHat, APNIC, The Sizzle, O'Reilly, CAVAL, and OneQode. We could not have delivered this conference without you. 

And last, but not least, we're thankful to **you** - our Delegates and Speakers. Thank you for submitting talks and the many hours it takes to prepare and practice them. Thank you for convincing your bosses to send you. Thank you for sending yourselves. Thank YOU. 

### Everything Open Adelaide 2025

Everything Open heads to Adelaide in January 2025, and we will be announcing specific dates and venue details in the coming weeks. 

If you'd like to be a part of the Adelaide team, we're always on the lookout for skilled and dedicated Organisers willing to give around an hour or two a week in the months leading up the conference - just drop us a line at [contact@everythingopen.au](mailto:contact@everythingopen.au).

Keep an eye on the [Everything Open](https://everythingopen.au) website for more information. 

You can keep up to date with all the Everything Open happenings in the following ways:

{% include socials.html %}

