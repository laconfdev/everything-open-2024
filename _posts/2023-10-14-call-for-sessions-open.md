---
layout: post
title: "Call for Sessions Now Open"
card: sessions.eb725e89.png
---

<p class="lead">Submit your session proposals today - the Everything Open 2024 Call for Sessions is now open.</p>

## Call for Sessions

We invite you to submit a session proposal on a topic you are familiar with via our [proposals portal](../../programme/proposals/).
The Call for Sessions will remain open until 11:59pm on Sunday 19 November 2023 anywhere on earth (AoE).

There will be multiple streams catering for a wide range of interest areas across the many facets of open technology, including Linux, open source software, open hardware, standards, formats and documentation, and our communities.
In keeping with the conference’s aim to be inclusive to all community members, presentations can be aimed at any level, ranging from technical deep-dives through to beginner and intermediate level presentations for those who are newer to the subject.

There will be two types of sessions at Everything Open: talks and tutorials.
Talks will nominally be 45 minutes long on a single topic presented in lecture format.
We will also have a few short talk slots of 25 minutes available, which are perfect for people new to presenting at a conference.
Tutorials are interactive and hands-on in nature, presented in classroom format.
Each accepted session will receive one Professional level ticket to attend the conference.

The Session Selection Committee is looking forward to reading your submissions.
We would also like to thank them for coming together and volunteering their time to help put this conference together.

## Sponsor Early

As usual, we have a range of sponsorship opportunities available, for the conference overall as well as the ability to contribute towards specific parts of the event.
We encourage you to sponsor the conference early, to get the maximum promotion during the lead up to the event.
If you or your organisation is interested in sponsoring Everything Open, please [get in touch](../../sponsors/prospectus/).

