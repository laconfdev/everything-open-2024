---
layout: post
title: "Announcing Everything Open 2024 and 2025"
card: launch.2114e30a.png
---

<p class="lead">Australasia's grassroots Free and Open Source technologies conference, Everything Open, will be returning in 2024 and 2025!</p>

Linux Australia is pleased to announce that Everything Open will be be held in Gladstone, Australia from April 16-18 2024.
This three day conference will have presentations on a range of open technologies topics from community members and project leaders.
As usual, there will be a key focus on Linux, open source software and open hardware, as well as the communities that surround them.

We are also happy to announce that we will be coming to Adelaide, Australia in January 2025.
While planning is still in its infancy for this event, we are keen to give some certainty over dates well in advance.
Further details will be shared about the 2025 conference following the Gladstone event.

Everything Open is a grassroots conference with a focus on open technologies, the community that has built up around this movement and the values that it represents.
The presentations will cover a broad range of subject areas including Linux, open source software, open hardware, open data, open government, and open GLAM (galleries, libraries, archives and museums), to name a few.
As we have come to expect, there will be technical deep-dives into specific topics from project contributors, as well as tutorials on building hardware or using a piece of software, not to mention talks covering the inner workings of our communities.

## Call for Sessions

The Everything Open 2024 Call for Sessions will open on 14 October 2023.
We encourage you to start thinking about talks to present at the conference, ready to submit a proposal once the dashboard opens.

## Sponsorship opportunities

We have a wide range of sponsorship opportunities available.
In addition to sponsoring the conference overall, there are a number of opportunities to contribute towards specific parts of the event.
If you or your organisation is interested in sponsoring Everything Open, please [get in touch](../../sponsors/prospectus/).

## Stay in the know

Subscribe to our announcement mailing list and follow our social channels to be the first to know about ticket sales, keynote speakers and conference highlights.

* [Mailing list](https://lists.linux.org.au/mailman/listinfo/eo-announce)
* [Mastodon](https://fosstodon.org/@EverythingOpen)
* [LinkedIn](https://www.linkedin.com/showcase/everythingopen/)
* [Conference website](https://2024.everythingopen.au/)
