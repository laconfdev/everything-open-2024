---
layout: post
title: "Keynote announcement - Professor Aaron Quigley"
card: card-keynote-quigley.png
---

<p class="lead">We are delighted to announce our third Keynote - Professor Aaron Quigley!</p>

## About Professor Aaron Quigley

Aaron Quigley is the [Science Director and Deputy Director of CSIRO’s Data61](https://www.csiro.au/en/news/all/articles/2023/january/welcome-professor-aaron-quigley/). From 2020 – 2023 he was head of school for the [School of Computer Science and Engineering (CSE) in the University of New South Wales](https://www.engineering.unsw.edu.au/computer-science-engineering) in Sydney Australia and until June 2020, Professor Quigley was the Chair of Human Computer Interaction in the [School of Computer Science](http://www.cs.st-andrews.ac.uk/) at the [University of St Andrews](http://www.st-andrews.ac.uk/), director of the [Scottish Informatics and Computer Science Alliance (SICSA)](https://www.sicsa.ac.uk/), board member for [ScotlandIS](https://www.scotlandis.com/) and the [DataLab](https://www.thedatalab.com/).

{% include keynote_image.html url="../../media/img/keynotes/aaron-quigley.jpg" description="Image: Aaron Quigley (supplied)" %}

Aaron’s research interests include discreet computing, global HCI, pervasive and ubiquitous computing and information visualisation on which he has delivered over 50 invited talks most recently as an ACM Distinguished Speaker. Aaron has [published over 190 internationally peer-reviewed publications](http://orcid.org/0000-0002-5274-6889) including edited volumes, journal papers, book chapters, conference and workshop papers.

[Aaron is an ACM Distinguished Member](https://awards.acm.org/award_winners/quigley_6729707) and an IEEE Senior Member. Aaron was the technical program Chair for the ACM EICS 2022 conference and general co-chair for the [ACM CHI Conference on Human Factors in Computing Systems in 2021](https://chi2021.acm.org/). He serves as chair elect on the [ACM CHI Steering committee](http://www.chi.acm.org/) and on the [Yirigaa Advisory Board](https://yirigaa.com.au/2020/12/yirigaa-is-thrilled-to-announce-the-appointment-of-professor-aaron-quigley-as-an-esteemed-member-to-our-advisory-board/). In total Aaron has had chairing roles in thirty international conferences and has served on over ninety conference and workshop program committees.

His research and development has been supported by the EPSRC, AHRC, JISC, SFC, NDRC, EU FP7/FP6, SFI, Smart Internet CRC, NICTA, Wacom, IBM, Intel, Nvidia, Google, Microsoft and MERL and has held 7 patents. Aaron has held academic and industry appointments in Singapore, Australia, Japan, USA, Germany, Ireland and the UK.

## About the Keynote - Intelligent Interfaces: Challenges and Opportunities

The exploration of novel sensing to facilitate new interaction modalities is an active research topic in Human-Computer Interaction. Across the breadth of HCI, we can see the development of new forms of interaction underpinned by the appropriation or adaptation of sensing techniques based on the measurement of sound, light, electric fields, radio waves, biosignals etc. In this talk, Professor Quigley will delve into a range of novel interactions which are supported by new forms of sensing on mobile and wearable devices.

## Register now for Everything Open 2024!

[Register now](../../dashboard/) to attend Everything Open 2024, in beautiful Gladstone, tropical north Queensland. We have a range of [ticket prices](../../attend/tickets/) suitable for most budgets, and have compiled [information on travel to, and accommodation in Gladstone](../../attend/travel-accommodation/), as well as [information on what you can do while you're here](../../attend/gladstone/). We're looking forward to seeing you in April 2024!

<a href="../../dashboard/" class="btn btn-primary" role="button">Register now!</a>
<a href="../../attend/tickets/" class="btn btn-secondary" role="button">View ticket prices</a>

You can keep up to date with all the Everything Open happenings in the following ways:

{% include socials.html %}

## Sponsor Early

As usual, we have a range of sponsorship opportunities available, for the conference overall as well as the ability to contribute towards specific parts of the event. We encourage you to sponsor the conference early, to get the maximum promotion during the lead up to the event. If you or your organisation is interested in sponsoring Everything Open, please [get in touch](../../sponsors/prospectus/).

<a href="../../sponsors/prospectus/" class="btn btn-primary" role="button">Sponsor Everything Open 2024</a>

