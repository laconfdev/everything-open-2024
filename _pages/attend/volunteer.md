---
layout: page
title: Volunteer
permalink: /attend/volunteer/
sponsors: true
---

Everything Open is a grass-roots, community-driven event, which wouldn't be successful without the wonderful people who are willing to lend us a hand.
This year, we're again seeking volunteers to fulfil a variety of roles to make this conference a success.

Volunteering at Everything Open is a great opportunity and can be deeply rewarding.

On a more practical note, volunteering at Everything Open can add to your professional portfolio in many ways, making this an ideal opportunity for students or recent graduates.

By volunteering at Everything Open, you will have the benefit of:

* Meeting exceptional people - many of whom are recognised experts in their field and industry luminaries
* Great networking opportunities with people working in the free and open source community
* Gaining practical, hands-on experience and skills in event management, customer liaison, public relations and operation of audio visual equipment - which are invaluable to future employers

## What's required of me?
To be eligible for Volunteer Registration at Everything Open you need to be:

* willing to commit to three full days of volunteering
* able to attend the mandatory training and induction prior to the conference
* able to arrive at the conference at 8am each conference day, depending on the role

The following is a indication of the tasks available across the conference:

<div class="table-responsive">

<table class="table table-striped table-bordered" summary="This table provides a summary of the days volunteers will be needed">
  <tr>
    <th>Day</th>
    <th>Tasks available</th>
  </tr>
  <tr>
    <td>Monday 15 April</td>
    <td>Conference bump in (general set up duties)</td>
  </tr>
  <tr>
    <td>Tuesday 16 April</td>
    <td>Conference room monitoring, room announcer, AV and general assistance</td>
  </tr>
  <tr>
    <td>Wednesday 17 April</td>
    <td>Conference room monitoring, room announcer, AV and general assistance</td>
  </tr>
  <tr>
    <td>Thursday 18 April</td>
    <td>Conference room monitoring, room announcer, AV and general assistance</td>
  </tr>
  <tr>
    <td>Friday 19 April</td>
    <td>Conference bump out (general clean up duties)</td>
  </tr>
</table>

</div>

**We would also like you to have one or more of the following skills and/or aptitudes:**

* the ability to meet and greet people and provide assistance with a friendly, cheerful disposition
* some familiarity with audio visual technologies
* confidence addressing an audience, such as introducing a speaker
* some familiarity with technology such as chat systems, online document sharing, social media, etc

## Role Details

* Room announcer
  * Prior to the conference, fill out speaker notes in the run sheets to decide how to introduce each speaker
  * Chat to the speaker(s) 10-15 minutes before the talk starts to confirm how they wish to be introduced, whether they want to take questions, etc.
  * Introduce speakers to the audience at the beginning of each presentation
  * Time keeping for the talk: give 5, 2, 1-minute warnings
  * Thank speakers at the end
* Audio/Visual
  * Look after video switching for each stream, perform tech checks to ensure speakers can present with decent quality audio
  * Volunteers with previous skills and experience in AV related work preferred
* Room Monitors
  * Responsible for monitoring the rooms within the conference venue, including watching the chat stream for issues and queries
  * Field general questions about the conference
  * Escalate to core team where necessary

## What's in it for me?

Tempted, but want to know more about what you get for volunteering your time?

This is what we're offering:

* Free conference registration at Hobbyist level
* Lunch provided during the conference
* Written reference covering your experience and duties - useful for job interviews!

## Sign me up!

<a href="https://forms.gle/Vfnu7nSfuNHkMQxn6" class="btn btn-primary" role="button">Apply to be a Volunteer at Everything Open 2024 now!</a>

Any questions? Email us at [volunteers@everythingopen.au](mailto:volunteers@everythingopen.au)
