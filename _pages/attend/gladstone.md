---
layout: page
title: Things to do when you're in Gladstone
permalink: /attend/gladstone/
sponsors: true
---

Once you're [here in Gladstone](/attend/travel-accommodation/), you'll be happy to know there's lots of things to do.

## Map
{% include gladstone_map.html %}

## Sightseeing

* Eastern Shores Water Play Park - Family-friendly water park with sprinklers, open 9am to 6pm daily during April.
* [Quoin Island Retreat and Turtle Rehabilitation Centre](https://www.gladstoneregion.info/explore/gladstone/attraction/quoin-island-retreat-and-turtle-rehabilitation-centre/) - As the home to Quoin Island Turtle Rehabilitation Centre, Quoin Island offers an educational experience at the Turtle Centre encompassing environmental, team building and animal welfare experiences. Please note Quoin Island is accessible for private bookings only.
* [Tondoon Botanic Gardens](https://www.gladstone.qld.gov.au/tondoon-botanic-gardens) - Tondoon houses three main living collections, The Port Curtis Plant Region Collection, The South East Queensland Rainforest collection, and The North Queensland Rainforest collection.
* [Heron Island Ferry Terminal (to Heron Island)](https://www.heronisland.com/stay/getting-here) - Heron Island is famous around the world for its spectacular coral reef, extraordinary variety of aquatic life, nesting turtles, migrating whales and as a breeding sanctuary for an abundance of bird life. Ferry is from $AUD 170 return.
* Spinnaker Park - Fantastic spot for a walk or a picnic. Public toilets and BBQ facilities available.
* [Gladstone Regional Art Gallery and Museum](https://gragm.qld.gov.au/) - The Art Gallery and Museum preserves the region's cultural heritage, and exhibits works by local and Australian artists.

[Read the official destination guide for Gladstone](https://www.gladstoneregion.info/explore/) and the [Gladstone Regional Council's guide of things to do in Gladstone](https://www.gladstone.qld.gov.au/glad-gladstone#things-to-do).

## Food and beverage

There's something for everyone. Please see the [map](#map) above for more eateries and drinkeries.

_Have we missed something important? Is your venue or accommodation not listed? Let us know at [contact@everythingopen.au](mailto:contact@everythingopen.au)._

