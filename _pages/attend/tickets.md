---
layout: page
title: Tickets
permalink: /attend/tickets/
sponsors: true
---

## Ticket Information

### Purchase your ticket

To purchase a ticket, please login to your [Dashboard](/dashboard/).
Once you are logged in you will have an option to purchase a ticket on the dashboard page.
Payment is by Visa, Mastercard, or American Express cards only.

<a href="/dashboard/" class="btn btn-outline-primary" role="button">Go to Dashboard</a>

If you have any special requirements or concerns that are not captured in our registration process please don't hesitate to get in touch with us.

### Key Dates

 * Registrations Open: Late 2023
 * Early Bird Registration: **Closes Jan 31 2024, while stock lasts**
 * Conference Opens: 16 April 2024

### Attendee Expectations

All attendees of the conference must agree to abide by the conference's [Code of Conduct](/attend/code-of-conduct/).
The conference aims to provide a safe and welcoming environment for all attendees and we take the Code of Conduct seriously.

All ticket purchases are subject to the conference [Terms and Conditions](/attend/terms-and-conditions/).

Please familiarise yourself with the [Health Statement](/attend/health-statement/) as to the precautions Everything Open is taking during the event.

## Prices and inclusions

All prices quoted are in Australian Dollars and include 10% Australian Goods and Services Tax (GST).

{% include ticket_matrix.html %}

## Ticket types

### Contributor

The Contributor ticket is designed for those who wish to attend Everything Open as a Professional delegate, as well as provide further financial support for the conference.
In recognition of this support, Contributors have the option to get their logo and a link to their organisation published on the Contributors page and will be acknowledged during the conference opening.

### Professional

The Professional ticket is the standard full inclusion conference ticket.
This rate applies to most people who have their companies pay the conference fees, or for individuals who can otherwise afford to support the conference at this level.

We ask that you choose this ticket if your company is paying for you to attend.

### Hobbyist

The Hobbyist rate is heavily discounted for open source enthusiasts who are paying out of their own pockets and would otherwise find it difficult to attend.

### Student

This is a concession rate ticket that is reserved for High School, College, TAFE or University Students.
Everything Open offers this rate as a form of investment in the future of the open technology community.
As part of the registration process, a valid student ID card or proof of enrolment may be requested by the conference organisers.
Any Student who cannot provide this if requested will be required to register as a hobbyist by paying the difference in fees between the Student rate and the Hobbyist rate.

### Single Day

This ticket gives access to a single day of the conference only.
Delegates with this ticket level will be able to attend presentations on the day they have selected, but will not be able to attend other events or other days of the conference without having an appropriate additional ticket.

### Online 

This ticket provides you with access to a live, online stream of the conference, so that you can watch all the presentations as they happen. Connectivity details for the online streaming platform will be provided prior to the conference. 

## Other information

### Volunteers

This conference would not be as successful as it is without having a dedicated team of volunteers to help run the conference.
If you're interested in spending some time helping run the conference, please [apply to be a volunteer](/attend/volunteer/).

### Media

There are a limited number of Media Passes available to media personnel.
Media Passes are free of charge, and entitle media personnel to attend the conference with all the entitlements of a Professional registration.
Please note, due to the limited numbers of Media Passes available, all Media Passes will need to be approved by the organising team.

To apply for a Media Pass, please [contact our team](mailto:contact@everythingopen.au).

### Go Green and Reduce your Carbon Footprint

When purchasing your ticket there is an option to purchase trees to reduce your carbon footprint, thanks to [Fifteen Trees](https://15trees.com.au/).
