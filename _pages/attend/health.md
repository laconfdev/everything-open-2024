---
layout: page
title: Health Statement
permalink: /attend/health-statement/
sponsors: true
---

_Last Updated 27 September 2023_

Linux Australia is committed to taking all reasonable steps to provide the safest possible environment for onsite participation at Everything Open 2024 by delegates, speakers, guests, and suppliers.
All attendees are required to be aware of and comply with the COVID-19 safety guidelines and protocols in place at that time.
Linux Australia reserves the right to deny access to the event to anyone who does not comply.

In light of ongoing COVID-19 concerns, we strongly encourage the wearing of masks while at the conference.
We remind all attendees to take steps to social distance where possible and to undertake sound hygiene practices throughout the conference.
Ensuring you are fully vaccinated is another way to minimise the risks associated with COVID-19.
Hand sanitisers will be available at the venue and masks will be available at the Registration Desk.
You are also more than welcome to bring your own mask to wear during the event.

Importantly, if you are not feeling well and are displaying COVID-19 symptoms, please do not attend the conference.
We suggest you test accordingly for COVID-19 and notify the [conference organising team](mailto:contact@everythingopen.au) immediately to advise you will not be in attendance.
As outlined in the conference [Terms and Conditions](/attend/terms-and-conditions/), a full refund will be offered in this instance.

Linux Australia commits to not reducing these restrictions between now and the event.
If changes are necessary, they will only be made to add further restrictions in order to provide a safe event for all attendees.
