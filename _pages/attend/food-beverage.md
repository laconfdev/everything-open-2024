---
layout: page
title: Food and beverage options
permalink: /attend/food-beverage/
sponsors: true
---

There are many food and beverage outlets close to the conference venue, for breakfast, lunch or an evening meal.

{% assign theVenues = site.data.food %}
{% assign theVenues = theVenues | sort: 'distance' %}
{% for venue in theVenues %}
<div class="venue">

{% if venue.image_url %}
<figure class="figure venue-figure">
    <img src="{{ venue.image_url }}" alt="{{ venue.title }}" class="img venue-img">
</figure>
{% endif %}

{% if venue.url %}
<h2><a href="{{ venue.url }}">{{ venue.name }}</a></h2>
{% else %}
<h2>{{ venue.name }}</h2>
{% endif %}

{% if venue.subtitle %}
<h3>{{ venue.subtitle }}</h3>
{% endif %}

{% if venue.address %}
<p>{{ venue.address }}</p>
{% endif %}

{% if venue.blurb %}
<p>{{ venue.blurb }}</p>
{% endif %}

<p>
{% if venue.distance %}
Distance from venue: {{ venue.distance }}m &nbsp;
{% if venue.google_maps_url %}
<a href="{{ venue.google_maps_url }}" target="_blank">View on map</a>&nbsp;</p>
{% else %}
</p>
{% endif %}
{% endif %}

<p>
{% if venue.Vegetarian %}
<span class=VenueOptionAvailable>Vegetarian options available</span>
{% else %}
<span class=VenueOptionNotAvailable>Vegetarian options not available</span>
{% endif %}

{% if venue.Vegan %}
<span class=VenueOptionAvailable>| Vegan options available</span>
{% else %}
<span class=VenueOptionNotAvailable>| Vegan options not available</span>
{% endif %}

{% if venue.GlutenFree %}
<span class=VenueOptionAvailable>| Gluten Free options available</span>
{% else %}
<span class=VenueOptionNotAvailable>| Gluten Free options not available</span>
{% endif %}

{% if venue.DairyFree %}
<span class=VenueOptionAvailable>| Dairy Free options available</span>
{% else %}
<span class=VenueOptionNotAvailable>| Dairy Free options not available</span>
{% endif %}


{% if venue.Halal %}
<span class=VenueOptionAvailable>| Halal options available</span>
{% else %}
<span class=VenueOptionNotAvailable>| Halal options not available</span>
{% endif %}
</p>

</div>
{% endfor %}





