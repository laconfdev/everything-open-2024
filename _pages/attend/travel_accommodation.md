---
layout: page
title: Travel to, and Accommodation in Gladstone
permalink: /attend/travel-accommodation/
sponsors: true
---

We're delighted to be hosting `#EverythingOpen` in a regional area. Simply put, if you're an open source developer, open hardware hacker, open data
designer, open GLAM practitioner or open education specialist living and working in Australia's regions, then it's harder to connect and collaborate
with like-minded people.

Everything Open is for Every**one** Open and the good news is that getting to [Gladstone](https://www.gladstone.qld.gov.au/tourism) is easier than you
might think!

## About Gladstone

The [Traditional Owners](https://www.pccctrust.com.au/) of the Gladstone area are the Port Curtis Coral Coast People, which comprise four groups: the
Gurang, [the Gooreng Gooreng](https://en.wikipedia.org/wiki/Goreng_Goreng), the Bailai, and the [Taribelang Bunda](https://taribelang.com.au/).
These Indigenous groups have deep connections to the land, culture, and community in the region.

Gladstone is tropical, and you can expect the [weather to be in the mid-20s and a little humid](http://www.bom.gov.au/climate/averages/tables/cw_039041.shtml).

It's a 560 km, 6 to 7-hour drive from Brisbane depending on the route you take, there are daily flights from [Brisbane Airport](https://www.bne.com.au/) (BNE)
to [Gladstone Airport](https://gladstoneairport.com.au/) (GLT) which take around an hour, and twice-weekly flights from Tullamarine (MEL) with Bonza.

You could even ride the 160kph Tilt Train, or one of the [more luxuious trains](#trains) as a travel experience.  (**Important Note:** See below
for [track works](#planned-track-maintenance-11th-to-16th) that have been tentatively scheduled at the start of the Conference.)

Everything Open will be held in the [Gladstone Entertainment and Convention Centre](https://www.gladstoneentertainment.com/) - please note this is
a different venue to the Gladstone _Events_ Centre.

There are plenty of [accommodation](#Accommodation) options to choose from, [eateries to explore](/attend/gladstone/#food-and-beverage), and
[sights](/attend/gladstone/#sightseeing) to see.

* [Map](#map)
* [Travel](#travel)
* [Accommodation](#accommodation)
* [Sightseeing](/attend/gladstone#sightseeing)
* [Food and beverage](/attend/gladstone/#food-and-beverage)

## Map
{% include gladstone_map.html %}

## Travel

### Special charter bus from Brisbane to Gladstone 

We're chartering a bus from Brisbane to Gladstone return to help get you to Everything Open 2024! 

You can read [more details about the bus here](/news/brisbane-gladstone-bus/), but in summary, the bus will pick up at Brisbane airport, drop you near the venue in Gladstone, then drop you back near Brisbane airport on the return journey. The bus will cost $AUD 40 each way. You buy tickets through the Dashboard.  

<a href="/dashboard/" class="btn btn-primary" role="button">Buy bus tickets now</a>

### Planes

When you arrive at Gladstone airport, it's a 10-minute taxi or Uber ride into central Gladstone and you can expect to pay $AUD 15-20.

### Brisbane BNE to Gladstone GLT

The flight time from Brisbane to Gladstone is around an hour.

[Qantas](https://www.qantas.com/au/en.html) operates 6 flights a day between Brisbane and Gladstone, ranging in price from $AUD 240 to $AUD 300 one way
or around $AUD 500 to 600 return.

[Virgin](https://book.virginaustralia.com) operates 2 flights a day between Brisbane and Gladstone, and you can expect to pay $AUD 200 to $AUD 300 one way
or around $AUD 400 to $AUD 600 return.

### Melbourne MEL

[Bonza](https://www.flybonza.com/) operate flights from Melbourne (Tullamarine) twice a week. You will need the Bonza app to check and book flights.

Virgin and Qantas do not fly direct from Melbourne and you will need to fly via Brisbane.

### Sydney SYD

There are currently no direct flights from Sydney to Gladstone, so you will need to fly via Brisbane.

### Trains

The [Tilt Train](https://www.queenslandrailtravel.com.au/railexperiences/ourtrains/tilttrain) will be running a reduced service of
only one trip per day, instead of their normal two, due to major maintenance on the high-speed trains themselves.

There are also other trains for a more relaxed experience, such as [The Spirit of Queensland](https://www.queenslandrailtravel.com.au/railexperiences/ourtrains/spiritofqueensland)
which runs between Brisbane and Cairns, and [The Spirit of the Outback](https://www.queenslandrailtravel.com.au/railexperiences/ourtrains/spiritoftheoutback)
which travels to Longreach.

#### Planned Track Maintenance (11th to 16th)
Unfortuantely, Queensland Rail have tentatively scheduled _track maintenance_ between the 11th and 16th of April, and are expecting to run
no trains leaving Brisbane **at all** over that period, replacing them with a Bus. This does mean that if anyone does want to experience the 160kph Tilt Train on
the way to Gladstone, they should organise their travel to be _before the 11th of April_.

There is currently no planned maintenance after that until the 26th of April.

For up to date information, please [see the Service Updates page](https://www.queenslandrailtravel.com.au/serviceupdates/Pages/default.aspx) and
scroll to the bottom of the page, and then select the 'Planned Maintenance' tab.

### Automobiles

If you are planning on driving, there are several picturesque routes from all over Australia - but be aware that from Melbourne, it's a 3-day drive.
You can find several suggested itineraries below if you're making Gladstone part of a longer road journey. You also need to be aware there are several
roadworks in operation between Brisbane and Gladstone, and that the nearby roads are populated by large trucks, tankers and heavy goods vehicles.

* [7-day around Gladstone road trip](https://www.gladstoneregion.info/blog/7-day-great-gladstone-road-trip/) - for those wanting to incorporate local sights into their journey
* [Gladstone Road Trips](https://www.queensland.com/au/en/plan-your-holiday/road-trips/gladstone-road-trips) - for those planning a longer holiday

### Greyhound Bus

[Greyhound](https://www.greyhound.com.au/buses/brisbane-to-gladstone) operates services between Brisbane and Gladstone twice daily. You can expect to
pay around $AUD 80-200 for a one way trip depending how far in advance you book. The bus takes around 12 hours, and arrives lates at night.

## Accommodation

Gladstone offers a range of accommodation options, catering to different needs.

* [Recommended accommodation](#recommended-accommodation)
* [Apartment-style accommodation](#apartment-style-accommodation)
* [Motel-style accommodation](#motel-style-accommodation)
* [Caravan and camping-style accommodation](#caravan-and-camping-style-accommodation)

### Recommended accommodation 

We recommend the following accommodation due to its proximity to the conference, or due to preferential pricing we have generously been given by accommodation providers. 

#### Highpoint International Hotel Pty Ltd & Dimitri’s Mediterranean Restaurant

Booking website: [https://www.highpointinternational.com.au](https://www.highpointinternational.com.au) | 22-24 Roseberry St, Gladstone, Queensland 4680 Phone: [07 4972 4711](tel:07 4972 4711)

This hotel is located on the same block as the Gladstone Entertainment and Convention Centre. It is closest to the conference venue. 

Rates for 2 people are as follows, correct as of January 2024. Extra person is $AUD 45 per night. 

* Level 1, 2 & 3 city side - $AUD 175 Queen Room with King Single and fold out Double Sofa Bed
* Level 3 ocean view & 4 city side - $AUD 180 Queen Room with King Single and fold out Double Sofa Bed
* Level 4 ocean view, 5 & 6 city side - $AUD 190 King Room
* Level 6 ocean view - $AUD 200 King Room
* Level 7 - $AUD 240 King Room

#### Central Apartment Group - Gladstone Central Plaza, Gladstone City Central, Gladstone Downtown Central, Curtis Central Apartments 

15% discount offered if booked directly through the website [https://centralapartmentgroup.com.au](https://centralapartmentgroup.com.au). When booking, please use the promo code `Linux24`. 

Prices are current as of January 2024. 

##### Gladstone Central Plaza

52 Oaks Lane, Gladstone Queensland 4680 

* Single Bedroom Apartment - $AUD 171
* Two Bedroom Apartment - $AUD 243

##### Gladstone City Central

66 Oaks Lane, Gladstone Queensland 4680

* Studio - $AUD 134
* One Bedroom Apartment - $AUD 178
* Two Bedroom Apartment	 - $AUD 238

##### Gladstone Downtown Central

39-43 Bramston Street, Gladstone Queensland 4680

* Studio - $AUD 131
* One Bedroom Apartment - $AUD 157
* Two Bedroom Apartment - $AUD 203

##### Curtis Central Apartments

30 Goondoon St, Gladstone Queensland 4680

* Two Bedroom Apartment - $AUD 214

#### Oaks Gladstone Grand Hotel

79 Goondoon St, Gladstone Queensland 4680

Booking website: [https://www.oakshotels.com/en/oaks-grand-gladstone/rooms](https://www.oakshotels.com/en/oaks-grand-gladstone/rooms)

This hotel is situated one block away from the conference venue. 

* One Bedroom Apartment - $AUD 200

### Apartment-style accommodation

* [Gladstone Reef Hotel](https://reefhotelgladstone.com.au/#panel-accommodation) - The ground floor accommodation here is ideal for those with mobility challenges.
* [Gladstone Capricorn Apartments](https://sonata.hotel-detail-check.com/gladstone-capricorn-apartments-australia#) - Self-contained queen studio apartments.
* [Apartments G60 Gladstone](https://www.apartmentsg60gladstone.com.au/) - 4-star apartment style, fully self-contained accommodation.

### Motel-style accommodation 

* [The Club Hotel Gladstone](https://theclubhotelgladstone.com.au/accommodation/) - This motel-style accommodation, with onsite parking, advertises that it is rainbow friendly.
* [Harbour Sails Motor Inn](https://harboursails.com.au/) - 4-star motel style accommodation with parking for larger vehicles.
* [Mid City Motor Inn](https://www.midcitymotorinn.com/) - Basic, affordable motel style accommodation, catering to families.
* [Harbour Lodge Motel](https://fear.vacationstay.eu/harbour-lodge-motel-australia) - 3-star motel accommodation.
* [The Queens Gladstone](https://www.queensgladstone.au/) - Basic motel-style accommodation, including family rooms.
* [CQ Motel Gladstone](https://southerncrossmotelgroup.com.au/motels/qld/gladstone-accommodation/gladstone-accommodation-cq-motel-gladstone/) - Motel-style accommodation.
* [Parkview Motel](http://www.parkviewmotelgladstone.com.au/) - Basic, motel-style accommodation.
* [Gladstone Backpackers](https://www.gladstonebackpackers.com/) - Cheap, hostel-style shared accommodation.

### Caravan and camping-style accommodation

If you are planning on bringing a recreational vehicle (RV) to Gladstone, you may also enjoy [this information on RV facilities around the region, compiled by Gladstone Regional Council](https://www.gladstone.qld.gov.au/rv-friendly-information). 

Please be aware that camping is no longer permissible at the Gladstone Showgrounds. Do not plan to camp at Gladstone Showgrounds.
{:.alert .alert-warning}

* [Kin Kora Village Caravan Park](https://www.kinkoravillagecaravanparkgladstone.com/) - Kin Kora offers a range of camping, caravan and cabin sites, some of which are pet-friendly. It's about an 8-10 minute drive from Kin Kora to the conference venue, and it is <em>not</em> walking distance. 

## Things to do in Gladstone

There's a wide variety of [things to do](/attend/gladstone/) when you're not at Everything Open, or having some downtime, from Eco-Tourism all
the way to Industrial Tours (PPE is usually required).
