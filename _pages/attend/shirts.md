---
layout: page
title: Shirts and Swag
permalink: /attend/shirts/
sponsors: true
---

We are pleased to offer a range of shirts and other items through Redbubble so you can get a memento of attending Everything Open 2024.

In past years we have provided shirts as part of the registration, however we often ended up with a lot of unused shirts that went to waste, and were unable to provide as wide a range as desired by delegates. 

By moving everything to an online store we can offer a wider range of items (shirts, laptop sleeves, bags, coffee mugs, etc) while minimising waste.

We look forward to seeing you with your Everything Open 2024 merchandise at the conference and on [social media](/about/contact/)!

<a href="https://www.redbubble.com/shop/ap/158192555" class="btn btn-outline-primary" role="button">Buy Shirts and Swag</a>
