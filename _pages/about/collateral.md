---
layout: page
title: Collateral
permalink: /about/collateral/
sponsors: true
---

## Help us spread the word about Everything Open

Want to help us let folks know about `#EverythingOpen?` Great! 

We've put together some collateral - but if you need something different, please [contact us](/about/contact/). 

### Posters

![Everything Open 2024 Poster](/media/img/collateral/eo2024-poster-thumbnail.png){:width="10%"}

We have posters suitable for printing. 

* [High resolution](/media/pdf/eo2024-generic-poster-hires.pdf) (40Mb)
* [Low resolution](/media/pdf/eo2024-generic-poster-lores.pdf) (8Mb)

### Newsletter collateral 

We've put some copy below that may be useful for newsletters. 

---
Everything Open is Australia’s premier grassroots open technologies conference covering, well, everything open - open source systems, including Linux, open hardware, open data, open GLAM, open government, open education, open cyber security, and even open artificial intelligence. 

This year, Everything Open will be held in [Gladstone](/attend/gladstone/), north Queensland, from 16th-18th April 2024 at the [Gladstone Entertainment Convention Centre](/attend/travel-accommodation). 

Everything Open 2024 will host over 50 expert presentations, including keynote speakers [Geoff Huston AM, Chief Scientist at APNIC](/news/keynote-geoff-huston/),internet architect, [Jana Dekanovska, Adversary Operations Practice Lead in cyber threat intelligence at Crowdstrike](/news/keynote-jana-dekanovska/), and [Professor Aaron Quigley, Deputy Director of CSIRO’s Data 61](/news/keynote-aaron-quigley/). We have an incredible Locknote yet to announce, too!

Going to Everything Open is an engaging development opportunity, where you'll get to make connections with other open practitioners, learn from the people building open tools used the world over, and gain fresh perspectives to take back to your organisation. [Ticket prices are a fraction of most industry conferences](/attend/tickets/) - thanks to our generous [sponsors, including Google and Red Hat](/sponsors/), and because our conference is entirely volunteer-run. Deep discounts are available for students.

Everything Open is auspiced by [Linux Australia](https://linux.org.au), the non-profit organisation representing Australia's free and open source communities. 

Learn more at: [https://2024.everythingopen.au](https://2024.everythingopen.au) or contact us [contact@everythingopen.au](mailto:contact@everythingopen.au) for more information or questions.

---

### Imagery 

![Banner image](/media/img/collateral/eo-banner.png){:width="100%"}

![Social media image](/media/img/collateral/eo2024.png){:width="100%"}