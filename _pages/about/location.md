---
layout: page
title: Location
permalink: /about/location/
sponsors: true
---

## The Location

Gladstone will be the host city for Everything Open 2024.

We would like to acknowledge the Bailai, the Gurang, the Gooreng Gooreng and the Taribelang Bunda people who are the traditional custodians of this land.

## The Venue

The [Gladstone Exhibition Convention Centre](https://www.gladstoneentertainment.com/) (GECC) is located in the centre of the city's entertainment precinct.
Everything Open will use the full venue to host the keynote and breakout sessions during the conference.

There are several options for accommodation within walking distance of the GECC.
The Gladstone City Library is also right across the road, for those who would like a quiet space to sit and relax.

For information on getting to and from Gladsone [please see our Travel and Accomodation page](/attend/travel-accommodation/)

<div class="map-responsive">
    <iframe width="425" height="350" src="https://www.openstreetmap.org/export/embed.html?bbox=151.23824357986453%2C-23.85128242705404%2C151.271288394928%2C-23.830536987507077&amp;layer=mapnik&amp;marker=-23.8409199355566%2C151.25476598739624" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=-23.8409&amp;mlon=151.2548#map=16/-23.8409/151.2548">View Larger Map</a></small>
</div>
