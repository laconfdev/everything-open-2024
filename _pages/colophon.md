---
layout: page
title: Colophon
permalink: /colophon/
sponsors: true
---

### Website Copy

We stand on the shoulders of giants, both previous organisers of Linux Australia and other events, including but not limited to:
 * [linux.conf.au 2020](https://lca2020.linux.org.au/)
 * [PyCon AU](https://2019.pycon-au.org/)

### Website Images

 * Gladstone CBD & East Shores - May 2023 - DJI Air 2S &copy; 2023 by Joe Perchard is licensed under [CC BY-SA 4.0 DEED](https://creativecommons.org/licenses/by-sa/4.0/deed.en)

### Website Design

Brand for linux.conf.au by [Tania Walker](https://www.taniawalker.com)
Adapted for Everything Open by the conference team.

This website is developed using free and open source software.

 * [Jekyll](https://jekyllrb.com/)
 * [Bootstrap](https://getbootstrap.com/)
