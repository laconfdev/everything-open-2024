---
layout: page
title: Sponsorship Prospectus
permalink: /sponsors/prospectus/
sponsors: false
---

## How to sponsor

Contact [sponsorship@everythingopen.au](mailto:sponsorship@everythingopen.au) for more information and a copy of our prospectus.

## Why Sponsor

### Overview

Sponsors of Everything Open have a unique opportunity to globally promote their brand to a large number of influential industry professionals.
One of the world's best free and open source technology conferences, Everything Open is regularly attended by over 600 delegates, more than half of whom attend as Professional Delegates.
Delegates travel from all over the world for a week of thought provoking keynotes from our internationally recognised invited speakers, high quality presentations and tutorials from the makers of these technologies, and connecting with other delegates in both organised networking sessions and spontaneous encounters in the hallways.

### Sponsor Early

Agreeing to sponsor Everything Open 2024 early is important.
The earlier you sign a sponsorship agreement, and we have a purchase order completed, the more time we can spend promoting your sponsorship.

### Diversity and Inclusion

Everything Open aims to be an inclusive event which welcomes diverse groups from all parts of the FLOSS (Free, Libre, Open Source Software) community to an environment of respect, tolerance, and encouragement.
Everything Open has an enforced [Code of Conduct](/attend/code-of-conduct/), with all complaints treated confidentially and seriously.

## Sponsorship Packages

All sponsors are acknowledged each day of the conference during the opening session.
If you are interested in contributing to making Everything Open a success, please reach out for more information.

### Emperor Penguin

Emperor Penguin is our highest level of sponsorship.

Sponsorship package includes:
* Identification as an Emperor Penguin sponsor of the conference
* 6 Professional Tickets
* 4 additional tickets at the early bird price
* 2 additional PDNS-only tickets to permit additional representation at the PDNS
* An opportunity to address the delegates during the conference opening
* Promotion via banners across the keynote stage, conference streaming platform, waiting room slides, and mentions via our numerous communication channels to our delegates and followers
* Promotion of your logo prominently on the conference website and every delegate badge

### King Penguin

Sponsorship package includes:
* Identification as a King Penguin sponsor of the conference
* 4 Professional Tickets
* 2 additional tickets at the early bird price
* Promotion via banners around the conference, waiting room slides and during the conference via the streaming platform
* Promotion of your logo on the conference website sponsors page

### Royal Penguin

Sponsorship package includes:
* Identification as a Royal Penguin sponsor of the conference
* 3 Professional Tickets
* 1 additional tickets at the early bird price
* Two additional PDNS-only tickets to permit additional representation at the PDNS
* Promotion via logo mentions during the daily opening
* Promotion of your logo on the conference website sponsors page

### Adelie Penguin

Sponsorship package includes:
* Identified as an Adelie Penguin sponsor of the conference
* 2 Professional Tickets
* 1 additional ticket at the early bird price
* Promotion via logo mentions during daily opening
* Promotion of your logo on the conference website sponsors page

### Fairy Penguin

Sponsorship package includes:
* Identified as a Fairy Penguin sponsor of the conference
* 1 Professional Ticket
* Promotion of your logo on the conference website sponsors page
* Entry level financial sponsor for companies who wish to provide support to the conference

## Additional Sponsorship Opportunities

Conference sponsors have the opportunity to contribute in a more targeted way on top of their general sponsorship.
There are a number of additional packages available, each with limited availability.

### Video Sponsor

The sponsor will be introduced at the conference as having funded video streaming to delegates.
Talks will be uploaded for later viewing by delegates and our wider community, and the video sponsor will have their logo included in the title slide.

### Outreach and Inclusion Sponsor

Contribute directly to increasing the diversity of the free and open source technology community by sponsoring our Outreach and Inclusion Programme.
This programme reaches out to diverse delegates, from a variety of backgrounds, throughout the community.
Outreach and inclusion sponsorship supports delegates from diverse backgrounds who have a significant contribution to make, but would otherwise be unable to attend Everything Open.

### Catering Sponsor

Contribute directly to the catering of the conference.
Morning and Afternoon Tea breaks will be catered, and your company will be mentioned as the sponsor of the food for that break.

### In-Kind Sponsorship

Organisations that make a substantial material contribution to the organisation of the conference are entitled to be listed as in-kind sponsors of the conference.
In-kind sponsors will be noted at the opening and closing plenary sessions of the conference, and will have their logo on the Sponsors page on the conference website.
Potential opportunities for in-kind sponsorship include:

* Funding the attendance and registration costs of a speaker invited to the conference.
* Providing equipment necessary to successfully run conference events.
* Donating prizes for giveaways at conference plenaries and social events.
* Providing collateral for the Conference delegate packs.

Benefits of in-kind sponsorship will be assessed on a case-by-case basis.
We'd love to hear about what you have in mind, just email [sponsorship@everythingopen.au](mailto:sponsorship@everythingopen.au).
