---
layout: page
title: Sessions
permalink: /programme/sessions/
sponsors: true
---

Everything Open runs over three days, from Tuesday through Thursday.
This will be made up of two types of sessions - talks and tutorials.

### Talks
Talks are 35-45 minute presentations on a single topic.
These are generally presented in lecture format and form the bulk of the available conference slots.

### Tutorials
Tutorials are 90 minute interactive learning sessions.
Participants will gain some hands on knowledge or experience in a topic.
These are generally presented in a classroom format and are interactive or hands-on in nature.
Some tutorials will require preparatory work to be done or some hardware to be purchased prior to their attendance - please check the individual tutorial descriptions for more information.
